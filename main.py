# Using flask to make an api
# import necessary libraries and functions
from flask import Flask, jsonify, request, render_template
from werkzeug.utils import secure_filename
from python_speech_features import mfcc
import scipy.io.wavfile as wav
import numpy as np
import scipy.signal as sps

app = Flask(__name__)
  
# optional HTML page for testing
@app.route('/')
def upload_file():
   return render_template('upload.html')

@app.route('/result', methods = ['GET','POST'])
def result():
    if(request.method == 'POST'):
        # print(request.headers)
        f = request.files.get('file')
        if f.filename != "":
            if(f.filename[-4:] != '.wav'):
                return jsonify({'status':'fail','code':'401','message':'File format must be .wav','data': ""})
            f.save(secure_filename(f.filename))

            samplerate = 16000
            winlen = 0.025
            winstep = 0.01
            nfilt = 26
            numcep = 13
            winfunc = np.hamming

            if request.form.get("framelength"):
                try:
                    winlen = float(request.form.get("framelength"))
                except:
                    return jsonify({'status':'fail','code':'402','message':'Invalid input for framelength','data': ""})
            if request.form.get("framestep"):
                try:
                    winstep = float(request.form.get("framestep"))
                except:
                    return jsonify({'status':'fail','code':'403','message':'Invalid input for framestep','data': ""})
            if request.form.get("nfilt"):
                try:
                    nfilt = int(request.form.get("nfilt"))
                except:
                    return jsonify({'status':'fail','code':'404','message':'Invalid input for numfilter','data': ""})

            if(winlen <= 0 or winstep <= 0 or nfilt <= 0):
                return jsonify({'status':'fail','code':'401','message':'Input must be greater than 0','data': ""})
            

            (rate,signal) = wav.read(f.filename)
            number_of_samples = round(len(signal) * float(samplerate) / rate)
            signal = sps.resample(signal, number_of_samples)

            try:
                mfcc_feat = mfcc(signal=signal,samplerate=samplerate,winlen=winlen,winstep=winstep,numcep=numcep,nfilt=nfilt,winfunc=winfunc).tolist()
            except:
                return jsonify({'status':'fail','code':'401','message':'Invalid input','data': ""})
            
            return jsonify({'status':'success','code':'200','message':'Calculation successful','data': mfcc_feat})
        else:
            return jsonify({'status':'fail','code':'401','message':'File not found','data': ""})
  
  
# driver function
if __name__ == '__main__':
  
    app.run(debug = True)