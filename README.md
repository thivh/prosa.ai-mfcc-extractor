# Prosa.Ai - MFCC Extractor

## Overview

Sebuah implementasi API untuk mengalkulasi fitur Mel Frequency Cepstral Coefficients (MFCC) dari sebuah berkas audio.

## Instruksi Penjalanan Server

1. Buka console pada directory utama folder (`..\prosa.ai-mfcc-extractor`)
2. Jalankan `.\env\Scripts\activate` untuk mengaktifkan virtual environment Python
3. Jalankan `python -m pip install -r requirements.txt` untuk menginstall package yang dibutuhkan
4. Jalankan `python main.py` untuk menjalankan server
5. Server akan berjalan pada alamat `http://127.0.0.1:5000`

## Cara Penggunaan API

1. Alamat yang digunakan untuk menggunakan API kalkukasi MFCC adalah `http://127.0.0.1:5000/result`
2. Jenis body yang digunakan adalah `form-data` yang berisi key-value:

    a. `file`: berkas audio dalam format .wav yang akan diproses

    b. `framelength`: panjang frame yang digunakan pada pemrosesan MFCC (opsional, default: 0.025)

    c. `framestep`: Besar pergeseran frame yang digunakan pada pemrosesan MFCC (opsional, default: 0.01)

    d. `nfilt`: Jumlah mel filter yang digunakan pada pemrosesan MFCC (opsional, default: 26)

3. Berikut adalah contoh request header yang digunakan:

```
POST /result HTTP/1.1
User-Agent: PostmanRuntime/7.29.0
Accept: */*
Postman-Token: bd050fda-17c9-4ec1-8530-92f3973fa9d6
Host: 127.0.0.1:5000
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Type: multipart/form-data; boundary=--------------------------392192393889107704078419
Content-Length: 54857
```

4. Berikut adalah contoh request body yang digunakan:

```
{
  "file":  {input.files[0]},
  "framelength": 0.025,
  "framestep":  0.01,
  "nfilt":  26
}
```

5. Berikut adalah contoh konfigurasi request menggunakan Postman:

![image](/screenshots/Screenshot%202022-05-15%20230745.png?raw=true)
![image](screenshots/Screenshot%202022-05-15%20224708.png?raw=true)

6. Berikut adalah contoh response berhasil yang diterima:

```
{
    "code": "200",
    "data": [
        [
            7.500782858464962,
            -23.1879587170833,
            21.599284410666538,
            -15.07059580898585,
            26.08236500121849,
            -4.7197430894340595,
            26.099799644846485,
            -8.581162423334204,
            16.164934966417125,
            -7.610541204590757,
            26.85243524604504,
            -4.388296883441685,
            8.682025137149717
        ],
        [
            7.927059102343006,
            -27.830180830327013,
            24.843440145012284,
            -18.90116723324194,
            23.231424015200364,
            -10.96745438202096,
            24.627825464893775,
            -16.620321003252297,
            11.386244689638094,
            -9.58625897310718,
            23.22085976003421,
            -2.488145576823471,
            11.03188877872818
        ],
    .
    .
    .
    .
        [
            3.5039288200178818,
            -22.4558083521388,
            -2.7841649518527314,
            -17.31010191539284,
            -1.91345701244621,
            -15.239379382789348,
            -7.872199236621042,
            -12.901593411416627,
            -3.717584840338754,
            -25.29474569635191,
            -8.876049154370849,
            -10.651413303813655,
            -2.843413535036298
        ],
        [
            3.603805856272303,
            -24.803992852745807,
            -2.645351361374565,
            -18.638021806747414,
            -0.9624124857890829,
            -12.506787146246793,
            -2.2224407385355294,
            -8.585325604756687,
            -3.215417316207557,
            -12.701332299796524,
            -2.9700497321702946,
            -19.017900289385395,
            -10.648344939304067
        ]
    ],
    "message": "Calculation successful",
    "status": "success"
}
```

7. Berikut adalah contoh response gagal yang diterima:

```
{
    "code": "401",
    "data": "",
    "message": "File format must be .wav",
    "status": "fail"
}
```

## Tambahan

Telah disediakan halaman HTML sederhana untuk mengetes API yang dapat diakses pada `http://127.0.0.1:5000`
